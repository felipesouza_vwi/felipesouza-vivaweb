<!doctype html>
<html lang="en">
<head>
    <?php
    include('../vinicius/templates/head.php');
    ?>
</head>
<body>
<?php
include('../vinicius/templates/nav.php');
?>
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="section-principal w-100">
            
            </div>
        </div>
    </div>
</section>
<section id="hero-photo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 color-section-photos">
                <div class="row my-3">
                    <div class="m-auto links-photo">
                        <a class="mr-2" href="">All</a>
                        <a class="mr-2" href="">Design</a>
                        <a class="mr-2" href="">Photography</a>
                        <a class="mr-2" href="">UI Design</a>
                        <a class="mr-2" href="">UX Design</a>
                        <a class="mr-2" href="">Brading</a>
                        <a class="mr-2" href="">Propotype</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-1.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-10.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-11.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-12.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-11.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-12.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-11.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-1.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-12.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-11.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-12.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3 col-3 img-transition border border-dark">
                        <div class="row">
                            <img src="https://demo.voidcoders.com/htmldemo/potoliaV2/main-files/assets/img/portfolio/portfolio4-11.jpg"
                                 height="100%" width="100%"
                                 alt="">
                            <div class="position-absolute text-img">hello</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('../vinicius/templates/footer.php');
?>
</body>
</html>