<section>
    <div class="bgbar"></div>
    <footer class="footer-section">
        <div class="container-fluid border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="d-block mt-3">
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Start</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Experimente, é Grátis</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Ajuda</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Treinamentos ao Vivo</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2 ml-1">
                            <img src="https://www.vivaintra.com/media/images/v2/logogray-2017.png"
                                 width="149px" class="mt-4"
                                 alt="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="d-block mt-3">
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Enterprise</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Comunicação</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Colaboração</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Produtividade</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Recursos Humanos</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Extranet</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Contato</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="d-block mt-3">
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Preços</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Blog</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Colaboração</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Comunicação</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Endomarketing</a></p>
                                </div>
                                <div class="col-md-12 ml-3 border-left-footer">
                                    <p><a href="#">Gestão de Pessoas</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="d-block mt-3">
                                <h4>Acompanhe o vivaintra</h4>
                                <h2>nas redes sociais</h2>
                                <div class="row">
                                    <div class="m-auto">
                                        <a class="color-footer" href="#"><i class="fab fa-facebook-square icon-footer"></i></a>
                                        <a class="color-footer" href="#"><i class="fab fa-youtube ml-4 icon-footer"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 mt-4 mb-4">
                        <div class="row">
                            <div class="col-lg-2 border-right-footer">
                                <div class="row">
                                    <div class="ml-auto border-radius-footer"></div>
                                    <span class="mr-auto"><a class="color-footer" href="#">Status do sistema</a></span>
                                </div>
                            </div>
                            <div class="col-lg-2 border-right-footer">
                                <span class="d-flex justify-content-center text-light"><a href="#">Segurança</a></span>
                            </div>
                            <div class="col-lg-2 border-right-footer">
                                <span class="d-flex justify-content-center text-light"><a href="#">Termos de Uso</a></span>
                            </div>
                            <div class="col-lg-2 border-right-footer">
                                <span class="d-flex justify-content-center text-light"><a href="#">Privacidade</a></span>
                            </div>
                            <div class="col-lg-2 border-right-footer">
                                <span class="d-flex justify-content-center text-light"><a href="#">Sobre Nós</a></span>
                            </div>
                            <div class="col-lg-2">
                                <span class="d-flex justify-content-center text-light"><a class="color-footer" href="#">Contato</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 mt-4 mb-4 border-footer">
                        <div class="row">
                            <div class="m-auto">
                                <img src="https://www.vivaintra.com/media/images/v2/vivaweb.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
